package Dev.materialdesign.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;

import Dev.materialdesign.R;
import Dev.materialdesign.config.ConfiguracaoFireBase;
import Dev.materialdesign.model.Restaurante;
import Dev.materialdesign.utils.MaskEditUtil;

public class CadastroRestaurante extends AppCompatActivity {

    private EditText razaoSocialRestaurante, emailRestaurante, senhaRestaurante, cepRestaurante, cnpjRestaurante, telefoneRestaurante;
    private FirebaseAuth autenticacao;
    private DatabaseReference cadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_restaurante);

        razaoSocialRestaurante = findViewById(R.id.text_nomeFantasiaRestaurante);
        emailRestaurante = findViewById(R.id.text_emailRestaurante);
        senhaRestaurante = findViewById(R.id.text_senhaRestaurante);
        cepRestaurante = findViewById(R.id.text_cepRestaurante);
        cnpjRestaurante = findViewById(R.id.text_cnpjRestaurante);
        telefoneRestaurante = findViewById(R.id.text_telefoneRestaurante);

        cepRestaurante.addTextChangedListener(MaskEditUtil.mask(cepRestaurante, MaskEditUtil.FORMAT_CEP));
        telefoneRestaurante.addTextChangedListener(MaskEditUtil.mask(telefoneRestaurante, MaskEditUtil.FORMAT_FONE));
    }

    public void validarCadastroRestaurante(View view) {

        //Recuperar valores dos campos
        String textRazaoSocial = razaoSocialRestaurante.getText().toString();
        String textEmail = emailRestaurante.getText().toString();
        String textSenha = senhaRestaurante.getText().toString();
        String textCep = MaskEditUtil.unmask(cepRestaurante.getText().toString());
        String textCnpj = cnpjRestaurante.getText().toString();
        String textTelefone = telefoneRestaurante.getText().toString();

        if (!textRazaoSocial.isEmpty()) {//
            if ( !textEmail.isEmpty()) {
                if ( !textSenha.isEmpty()) {
                    if ( !textCnpj.isEmpty()) {
                        if ( !textCep.isEmpty()) {
                            if ( !textTelefone.isEmpty()) {
                                Restaurante restaurante = new Restaurante();
                                restaurante.setRazaoSocial(textRazaoSocial);
                                restaurante.setEmail(textEmail);
                                restaurante.setCep(Integer.parseInt(MaskEditUtil.unmask(cepRestaurante.getText().toString())));
                                restaurante.setTelefone(Integer.parseInt(telefoneRestaurante.getText().toString()));
                                restaurante.setCnpj(Integer.parseInt(cnpjRestaurante.getText().toString()));
                                this.CadastroDadosRestaurante(restaurante);
                                restaurante.setSenha(textSenha);
                                this.CadastroRestauranteAuth(restaurante);
                            } else {
                                Toast.makeText(CadastroRestaurante.this,"Preecha o campo Telefone", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(CadastroRestaurante.this,"Preecha o campo CEP", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(CadastroRestaurante.this,"Preecha o campo CNPJ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(CadastroRestaurante.this,"Preecha o campo Senha", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(CadastroRestaurante.this,"Preecha o campo E-mail", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(CadastroRestaurante.this,"Preecha o campo Nome Fantasia", Toast.LENGTH_SHORT).show();
        }
    }

    public void CadastroRestauranteAuth(Restaurante restaurante) {
        autenticacao = ConfiguracaoFireBase.getFirebaseAutenticacao();
        autenticacao.createUserWithEmailAndPassword(
                restaurante.getEmail(), restaurante.getSenha()
        ).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    String excessao = "";
                    try {
                        throw task.getException();
                    } catch (FirebaseAuthWeakPasswordException e) {
                        excessao = "Digite uma senha mais forte";
                    } catch (FirebaseAuthInvalidCredentialsException e) {
                        excessao = "Digite um email valido";
                    } catch (FirebaseAuthUserCollisionException e) {
                        excessao = "Esta conta já foi cadastrada";
                    } catch (Exception e) {
                        excessao = "Erro ao cadastrar usuário: " + e.getMessage();
                        e.printStackTrace();
                    }

                    Toast.makeText(CadastroRestaurante.this,excessao, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CadastroRestaurante.this,"Sucesso ao Cadastrar o Restaurante", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    public void CadastroDadosRestaurante(Restaurante restaurante) {
        cadastro = ConfiguracaoFireBase.getFirebaseDatabase();

        cadastro.child("Restaurantes").push().setValue(restaurante);
    }
}
