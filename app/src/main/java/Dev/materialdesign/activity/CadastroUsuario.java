package Dev.materialdesign.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import Dev.materialdesign.config.ConfiguracaoFireBase;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;

import Dev.materialdesign.R;
import Dev.materialdesign.model.Usuario;
import Dev.materialdesign.utils.MaskEditUtil;

public class CadastroUsuario extends AppCompatActivity {

    private EditText nomeUsuario, emailUsuario, senhaUsuario, cepUsuario;
    private FirebaseAuth autenticacao;
    private DatabaseReference cadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);

        nomeUsuario = findViewById(R.id.text_nomeUsuario);
        emailUsuario = findViewById(R.id.text_emailUsuario);
        senhaUsuario = findViewById(R.id.text_senhaUsuario);
        cepUsuario = findViewById(R.id.text_cepUsuario);

        cepUsuario.addTextChangedListener(MaskEditUtil.mask(cepUsuario, MaskEditUtil.FORMAT_CEP));
    }

    public void validarCadastroUsuario(View view) {

        //Recuperar valores dos campos
        String textNome = nomeUsuario.getText().toString();
        String textEmail = emailUsuario.getText().toString();
        String textSenha = senhaUsuario.getText().toString();
        String textCep = MaskEditUtil.unmask(cepUsuario.getText().toString());

        if (!textNome.isEmpty()) {//
            if ( !textEmail.isEmpty()) {
                if ( !textSenha.isEmpty()) {
                    if ( !textCep.isEmpty()) {
                        Usuario usuario = new Usuario();
                        usuario.setNome(textNome);
                        usuario.setEmail(textEmail);
                        usuario.setCep(Integer.parseInt(MaskEditUtil.unmask(cepUsuario.getText().toString())));

                        this.CadastroDadosUsuario(usuario);//Chamo a função aqui para não cadastrar o usuário com a senha

                        usuario.setSenha(textSenha);

                        this.CadastroUsuarioAuth(usuario);
                    } else {
                        Toast.makeText(CadastroUsuario.this,"Preecha o campo Cep", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(CadastroUsuario.this,"Preecha o campo Senha", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(CadastroUsuario.this,"Preecha o campo email", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(CadastroUsuario.this,"Preecha o campo nome", Toast.LENGTH_SHORT).show();
        }
    }

    public void CadastroUsuarioAuth(Usuario usuario) {
        autenticacao = ConfiguracaoFireBase.getFirebaseAutenticacao();
        autenticacao.createUserWithEmailAndPassword(
            usuario.getEmail(), usuario.getSenha()
        ).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    String excessao = "";
                    try {
                        throw task.getException();
                    } catch (FirebaseAuthWeakPasswordException e) {
                        excessao = "Digite uma senha mais forte";
                    } catch (FirebaseAuthInvalidCredentialsException e) {
                        excessao = "Digite um email valido";
                    } catch (FirebaseAuthUserCollisionException e) {
                        excessao = "Esta conta já foi cadastrada";
                    } catch (Exception e) {
                        excessao = "Erro ao cadastrar usuário: " + e.getMessage();
                        e.printStackTrace();
                    }

                    Toast.makeText(CadastroUsuario.this,excessao, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CadastroUsuario.this,"Sucesso ao Cadastrar Usuário", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    public void CadastroDadosUsuario(Usuario usuario) {
        cadastro = ConfiguracaoFireBase.getFirebaseDatabase();

        cadastro.child("Usuarios").push().setValue(usuario);
    }
}
