package Dev.materialdesign.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import Dev.materialdesign.R;

public class Login extends AppCompatActivity {

    private Button btnCadastroUser, btnCadastroRestaurante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnCadastroUser = (Button) findViewById(R.id.btn_cadastro_avaliador);
        btnCadastroRestaurante = (Button) findViewById(R.id.btn_cadastro_restaurante);

        btnCadastroUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastroUserActivity();
            }
        });

        btnCadastroRestaurante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastroRestauranteActivity();
            }
        });
    }

    private void cadastroUserActivity() {
        startActivity(new Intent(Login.this, CadastroUsuario.class));
    }

    private void cadastroRestauranteActivity() {
        startActivity(new Intent(Login.this, CadastroRestaurante.class));
    }
}
